This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for "oidc-keycloak-library"

## [v1.0.1-SNAPSHOT]
- Prepared for user to roles alignment #22084

## [v1.0.0]
- First release (#19143, #19891)
