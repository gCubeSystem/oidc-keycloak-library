package org.gcube.oidc.keycloak.d4science;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.gcube.oidc.keycloak.d4science.ExportParser;
import org.xml.sax.SAXException;

public class ExportParserTest {

    public ExportParserTest() {
    }

    public static void main(String[] args)
            throws FileNotFoundException, SAXException, IOException, ParserConfigurationException {

        ExportParser ep = new ExportParser(new FileInputStream("src/test/resources/dev2-db.json"));
        System.out.println(ep.getAllContexts());
        System.out.println(ep.getAllUserContextsAndRoles());
    }
}
