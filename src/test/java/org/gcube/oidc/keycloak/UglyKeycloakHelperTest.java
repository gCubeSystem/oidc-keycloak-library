package org.gcube.oidc.keycloak;

import java.io.IOException;
import java.net.MalformedURLException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.ClientResource;
import org.keycloak.admin.client.resource.GroupResource;
import org.keycloak.admin.client.resource.PolicyResource;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.ResourceResource;
import org.keycloak.admin.client.resource.RoleResource;
import org.keycloak.common.VerificationException;
import org.keycloak.representations.idm.authorization.DecisionStrategy;
import org.keycloak.representations.idm.authorization.Logic;
import org.keycloak.representations.idm.authorization.ScopeRepresentation;

public class UglyKeycloakHelperTest {

    static String clientPrefix = "client";
    static String realm = "d4science";

    public UglyKeycloakHelperTest() {
    }

    public static void maino(String[] args) throws KeyManagementException, NoSuchAlgorithmException,
            VerificationException, MalformedURLException, IOException, KeycloakResourceCreationException {

        KeycloakHelper kh = KeycloakHelper.getInstance("https://nubis2.int.d4science.net/auth");
        //        KeycloakHelper kh = getInstance("http://localhost:8080/auth");
        Keycloak keycloak = kh.newKeycloakAdmin("admin", "4dm1n");
        //        Keycloak keycloak = keycloakHelper.newKeycloak(realm, "/gcube/devsec/devVRE",
        //                "12184fe2-f174-4c87-afac-b2d2bfaae4c0");

        //        RealmResource realmResource = kh.addRealm(keycloak, realm, realm, "<h2>" + realm + "</h2><p>Welcome</p>", true);
        RealmResource realmResource = keycloak.realm(realm);
        
        for (int clientNum = 0; clientNum < 10; clientNum++) {
            String clientName = clientPrefix + clientNum;
            ClientResource client = kh.addClient(realmResource, clientName, clientName, clientName, null);

            RoleResource dataManager = kh.addRole(client, true, "Data-Manager", "Data-Manager", "Data-Manager", null);
            RoleResource dataMinerManager = kh.addRole(client, true, "DataMiner-Manager", "DataMiner-Manager",
                    "DataMiner-Manager", null);
            ScopeRepresentation read = new ScopeRepresentation("read");

            ScopeRepresentation list = new ScopeRepresentation("list");
            ScopeRepresentation write = new ScopeRepresentation("write");
            ScopeRepresentation execute = new ScopeRepresentation("execute");
            Set<ScopeRepresentation> resourceScopes = new HashSet<>(Arrays.asList(read, write, list, execute));
            ResourceResource resource1 = kh.addResource(client, "resource1", null, "resource1", false, resourceScopes,
                    null);
            ResourceResource resource2 = kh.addResource(client, "resource2", null, "resource2", false, resourceScopes,
                    null);
            Set<String> resources = new HashSet<>(
                    Arrays.asList(resource1.toRepresentation().getName(), resource2.toRepresentation().getName()));

            Map<String, Set<String>> policyClientRoles = new HashMap<>();
            policyClientRoles.put(clientName, Collections.singleton(dataManager.toRepresentation().getName()));
            PolicyResource dataManagerCanRead = kh.addRoleResourcePolicy(client, resources,
                    Collections.singleton(list.getName()), "canRead", Logic.POSITIVE,
                    policyClientRoles);

            kh.addResourcePermission(client, resources, "read", DecisionStrategy.UNANIMOUS,
                    Collections.singleton(dataManagerCanRead.toRepresentation().getName()));

        }
        ////        keycloakHelper.addClient(realmResource, "Gino", "gino", "Gino client", "http://gino.stilla.it");
        //        UserResource user = keycloakHelper.findUser(realmResource, "mauro");
        //        Map<String, Object> impersonation = user.impersonate();
        //        System.out.println(impersonation);
        //      Keycloak keycloak = keycloakHelper.newKeycloak(realm, "lino", "lino", "portal");
        //      TokenManager tokenManager = keycloak.tokenManager();
        //      AccessToken accessToken = keycloakHelper.verifyAndGetToken(AccessToken.class, tokenManager.getAccessTokenString(), keycloakHelper.getRealmSigPublicKey(realm));
        //      System.out.println(new ObjectMapper().writeValueAsString(accessToken));
        //      for (String resourceAccess : accessToken.getResourceAccess().keySet()) {
        //          if ("account".equals(resourceAccess)) {
        //              continue;
        //          }
        //          Access access = accessToken.getResourceAccess(resourceAccess);
        //          System.out.println(resourceAccess + " -> " + access.getRoles());
        //      }
        //      keycloak.realm(realm).
    }

    public static void main(String[] args) throws Exception {
        KeycloakHelper kh = KeycloakHelper.getInstance("https://accounts.dev.d4science.org/auth");
        Keycloak keycloak = kh.newKeycloakAdmin("kadmin", "bb67fba2f32d3bd");
        RealmResource realmResource = keycloak.realm(realm);
        GroupResource groupResource = kh.findGroupByPath(realmResource, "gcube/devNext/NextNext");
        ClientResource clientResource = kh.findClient(realmResource, "/gcube");
        kh.mapGroupToCLientRole(groupResource, clientResource, "Member");
    }
}
