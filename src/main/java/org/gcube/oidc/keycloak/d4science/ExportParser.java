package org.gcube.oidc.keycloak.d4science;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class ExportParser {

    private ArrayNode rootNode;

    public ExportParser(FileInputStream exportFileFIS) throws SAXException, IOException, ParserConfigurationException {
        ObjectMapper objectMapper = new ObjectMapper();
        rootNode = (ArrayNode) objectMapper.readTree(exportFileFIS);
    }

    public Map<String, String> getAllUsersAndAvatars() {
        Map<String, String> users = new TreeMap<>();
        Iterator<JsonNode> arrayIterator = rootNode.elements();
        while (arrayIterator.hasNext()) {
            JsonNode entry = arrayIterator.next();
            users.put(entry.get("username").asText(), entry.get("avatarURL").asText());
        }
        return users;
    }

    public Set<String> getAllContexts() {
        Set<String> distinctContexts = new TreeSet<>();
        Iterator<JsonNode> arrayIterator = rootNode.elements();
        while (arrayIterator.hasNext()) {
            JsonNode entry = arrayIterator.next();
            ObjectNode contextsNode = (ObjectNode) entry.get("contexts");
            contextsNode.fieldNames().forEachRemaining(f -> distinctContexts.add(f));
        }
        return distinctContexts;
    }

    public Map<String, Set<String>> getContextsAndRoles(String user) {
        Iterator<JsonNode> arrayIterator = rootNode.elements();
        while (arrayIterator.hasNext()) {
            JsonNode entry = arrayIterator.next();
            String username = entry.get("username").asText();
            if (!user.equals(username)) {
                continue;
            }
            Map<String, Set<String>> contextAndRoles = new TreeMap<>();
            ObjectNode contextsNode = (ObjectNode) entry.get("contexts");
            Iterator<String> contextIterator = contextsNode.fieldNames();
            while (contextIterator.hasNext()) {
                String context = (String) contextIterator.next();
                Set<String> roles = new TreeSet<>();
                ArrayNode rolesNodes = (ArrayNode) contextsNode.get(context);
                rolesNodes.elements().forEachRemaining(r -> roles.add(r.asText()));
                contextAndRoles.put(context, roles);
            }
            return contextAndRoles;
        }
        return Collections.emptyMap();
    }

    public Map<String, Map<String, Set<String>>> getAllUserContextsAndRoles() {
        Map<String, Map<String, Set<String>>> usersToContextAndRoles = new TreeMap<>();
        Iterator<JsonNode> arrayIterator = rootNode.elements();
        while (arrayIterator.hasNext()) {
            JsonNode entry = arrayIterator.next();
            String username = entry.get("username").asText();
            Map<String, Set<String>> contextAndRoles = new TreeMap<>();
            ObjectNode contextsNode = (ObjectNode) entry.get("contexts");
            Iterator<String> contextIterator = contextsNode.fieldNames();
            while (contextIterator.hasNext()) {
                String context = (String) contextIterator.next();
                Set<String> roles = new TreeSet<>();
                ArrayNode rolesNodes = (ArrayNode) contextsNode.get(context);
                rolesNodes.elements().forEachRemaining(r -> roles.add(r.asText()));
                contextAndRoles.put(context, roles);
            }
            usersToContextAndRoles.put(username, contextAndRoles);
        }
        return usersToContextAndRoles;
    }
}