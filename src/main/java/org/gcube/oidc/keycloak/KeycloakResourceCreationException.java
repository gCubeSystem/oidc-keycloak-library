package org.gcube.oidc.keycloak;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.StatusType;

public class KeycloakResourceCreationException extends Exception {

    private static final long serialVersionUID = 4073975434440358303L;

    private StatusType responseStatus;
    private String responseContent;

    public KeycloakResourceCreationException() {
    }

    public KeycloakResourceCreationException(String message, Throwable cause, boolean enableSuppression,
            boolean writableStackTrace) {

        super(message, cause, enableSuppression, writableStackTrace);
    }

    public KeycloakResourceCreationException(String message, Throwable cause, Response response) {
        super(message, cause);
        if (response != null) {
            this.responseStatus = response.getStatusInfo();
            this.responseContent = response.readEntity(String.class);
        } else {
            this.responseStatus = null;
            this.responseContent = "<reponse not provided>";
        }
    }

    public KeycloakResourceCreationException(String message, Response response) {
        this(message, null, response);
    }

    public StatusType getResponseStatus() {
        return responseStatus;
    }

    public int getResponseStatusCode() {
        return responseStatus.getStatusCode();
    }

    public String getResponseContent() {
        return responseContent;
    }

    @Override
    public String getMessage() {
        return super.getMessage() + (getResponseStatus() != null
                ? "(REST details: [" + getResponseStatusCode() + "] " + getResponseContent() + ")"
                : "");
    }

}