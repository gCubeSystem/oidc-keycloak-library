# OIDC Keycloak Library

**OIDC Keycloak Library** provides some helpers that uses the [Keycloak](https://www.keycloak.org)'s clients Java API. The project also provide a command line program that generates all the clients related to a gCube infrastructure on a Keycloak server, starting from the infrastructure `Context-Users-Roles` JSON export.

## Structure of the project

The source code is present in `src` folder. 

## Built With

* [OpenJDK](https://openjdk.java.net/) - The JDK used
* [Maven](https://maven.apache.org/) - Dependency Management

## Documentation

To build the library JAR it is sufficient to type

    mvn clean package

### Launch the json import to REALM

In order to perform the import of an infrastructure JSON export file it's sufficient to type:

    mvn exec:java -Dexec.args="[keycloak-auth-base-url] [keycloak-admin-user] [keycloak-admin-pass] [realm-name] [json-export-path] [[avatar-base-url] [[avatars-target-folder]]]"

## Change log

See [Releases](https://code-repo.d4science.org/gCubeSystem/authorization-client/releases).

## Authors

* **Mauro Mugnaini** ([Nubisware S.r.l.](http://www.nubisware.com))

## How to Cite this Software
[Intentionally left blank]

## License

This project is licensed under the EUPL V.1.1 License - see the [LICENSE.md](LICENSE.md) file for details.

## About the gCube Framework
This software is part of the [gCubeFramework](https://www.gcube-system.org/ "gCubeFramework"): an
open-source software toolkit used for building and operating Hybrid Data
Infrastructures enabling the dynamic deployment of Virtual Research Environments
by favouring the realisation of reuse oriented policies.
 
The projects leading to this software have received funding from a series of European Union programmes see [FUNDING.md](FUNDING.md)

## Acknowledgments
[Intentionally left blank]